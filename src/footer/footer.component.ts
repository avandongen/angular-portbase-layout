import {Component, OnInit} from "@angular/core";

@Component({
    selector: 'pb-footer',
    template: `<div class="pcs-webpage-component-container">
                      <div class="mainfooter wrapped">
                        <a class="backtotop" (click)="moveToTop()" i18n="Back to top">Back to top</a>
                        <p>
                          Copyright Portbase bv - <span>S{{screenNumber}}</span>
                        </p>
                      </div>
                    </div>`,
    inputs: ['screenNumber']
})
export class FooterComponent implements OnInit {

    public screenNumber:string;

    constructor() {
    }

    ngOnInit() {
    }

    moveToTop() {
        window.scrollTo(0, 0);
    }
}
